#!/usr/bin/env python3
import itertools

with open("dled.py", "r") as f:
    dled = eval(f.read())

AGEMAP = {
    "U11": None,
    "U13": None,
    "U15": None,
    "U17": None,
    "U20": None,
    "Sen": "Senior",
    "V35": None,
    "V40": "40–44",
    "V45": "45–49",
    "V50": "50–54",
    "V55": "55–59",
    "V60": "60–64",
    "V65": "65–69",
    "V70": "70–74",
    "V75": "75–79",
    "V80": "80+",
}

EVENTMAP = {
    "High jump": "HJ",
    "Long jump": "LJ",
    "Pole vault": "PV",
    "Triple jump": "TJ",
    "Marathon": "Mar",
    "½ marathon": "HM",
    "⅓ marathon": "8.74M",
    "Mile (track)": "Mile",
    "Mile (road)": "Mile",
}


def eventmap(event):
    if event[-1] == "M":
        return event
    elif event[-2:] == "km":
        return event.replace("km", "K")
    elif event[-1] == "m":
        return event.replace("m", "")
    elif event[-3:] == "m H":
        return event.replace("m H", "H")
    elif event[-4:] == "m SC":
        return event.replace("m ", "")
    elif event in EVENTMAP:
        return EVENTMAP[event]


def TimetoSecs(time):
    """Convert a time string to seconds"""
    val = sum(
        (0 if x == "00" else float(x.lstrip("0"))) * 60 ** i
        for i, x in enumerate(time.strip().split(":")[::-1])
    )
    if val == int(val):
        return int(val)
    return val


with open("data.txt", "r") as dt:
    datatext = dt.read().strip()
datalines = [line.split("~") for line in datatext.split("\n")][1:]

# Get sorting data
with open("sortingdata.txt", "r") as sd:
    sdtext = sd.read().split("\n")
EVENTS = sdtext[3][6:].split(",")

combs = itertools.product("FM", AGEMAP.keys(), EVENTS)
badevents = set()
for c in combs:
    category = AGEMAP[c[1]]
    event = eventmap(c[2])
    if eventmap(c[2]) is None:
        badevents.add(c[2])
        continue
    if category is None:
        continue
    try:
        bestnew = dled[(c[0], category)][event]
    except KeyError:
        continue
    for row in datalines:
        if row[1] == c[0] and row[2] == c[1] and row[3] == c[2]:
            break
    else:
        print("Old: None")
        print("New: " + str(bestnew))
        print()
        continue
    if "m" in row[5]:
        if float(bestnew[3].replace("m", "")) > float(row[5].replace("m", "")):
            print("Old: " + str(row))
            print("New: " + str(bestnew))
            print()
        continue
    if TimetoSecs(bestnew[3]) < TimetoSecs(row[5].replace("s", "")):
        print("Old: " + str(row))
        print("New: " + str(bestnew))
        print()
print("Bad Events: " + ", ".join(badevents))
