#!/usr/bin/env python3
import os

path = "Records"
BREAKAT = 17
BARLENGTH = 80

if "PROFILING" not in vars():
    PROFILING = False

if not os.path.exists(path):
    os.makedirs(path)


class field:
    def __init__(self, name, isfilterable, visible):
        self.name = name
        self.isfilterable = isfilterable
        self.visible = visible


class afilter:
    def __init__(self, name, possibles, place):
        self.place = place
        self.name = name
        self.possibles = possibles


def findin(iterable, item):
    """Like list.index but catches ValueError"""
    try:
        return iterable.index(item)
    except ValueError:
        return -1


fields = []
filters = []

# Get data
with open("data.txt", "r") as dt:
    datatext = dt.read()
datalines = [line.split("~") for line in datatext.split("\n")]

while [""] in datalines:
    datalines.remove([""])

# Get sorting data
with open("sortingdata.txt", "r") as sd:
    sdtext = sd.read()
sdtext = sdtext.split("\n")
sortdict = dict()
sorts = []
for line in sdtext:
    if not line:
        continue
    n, L = line.split("=")
    L = L.split(",")
    sortdict.update({n: L})
    sorts.append((n, L))

# Extract fields
for fld in datalines[0]:
    fields.append(field(fld[2:], fld[0] == "1", fld[1] == "1"))

# Create entries
entries = datalines[1:-1]

# Create filters
for fld in range(0, len(fields), 1):
    if not fields[fld].isfilterable:
        continue
    possibles = set([ety[fld] for ety in entries])

    try:
        fieldsorter = sortdict[fields[fld].name]
        for d in fieldsorter:
            possibles.add(d)
        possibles = list(possibles)
        possibles.sort(key=lambda x: findin(fieldsorter, x))

    except KeyError:
        # Field is not sortable
        possibles = list(possibles)

    possibles.insert(0, "All")
    filters.append(afilter(fields[fld].name, possibles, fld))

with open("header.html", "r") as f:
    HEADER = f.read()

cpaths = [path]
# Create tree of directories
for ft in filters:
    ncp = []
    for ps in ft.possibles:
        for directory in cpaths:
            ncp.append(directory + "/" + ps)
    cpaths = ncp[:]

# Create HTML files
print("|" + "-" * BARLENGTH + "|\n|", end="")
for n, f in enumerate(cpaths):
    if round(n / len(cpaths) * BARLENGTH) > round((n - 1) / len(cpaths) * BARLENGTH):
        print("=", end="", flush=True)

    if PROFILING:
        if round(cpaths.index(f) * 100 / len(cpaths), 3) > 0.4:
            break

    name = f.replace(path, "", 1)
    name = name.split("/")[1:]

    # Determine is any of the all-whitespaces is in the path
    if any(part.isspace() for part in name):
        continue

    # Heading Table
    table = '<div style="overflow-x:auto"><table>'
    for ft in filters:
        # Create a row of filters
        table += f'<tr><th class="sticky">{ft.name}</th>'
        for ps in range(0, len(ft.possibles), 1):
            # Make a new row
            if ps % BREAKAT == 0 and ps > 0:
                table += '</tr><tr><th class="sticky"></th>'
            # Create the filter itself within the row
            linkto = name[:]
            linkto[ft.place] = ft.possibles[ps]
            linkto = "".join(linkto)
            linkto = "" * (len(filters) - 1) + linkto
            if f.split("/")[filters.index(ft) + 1] == ft.possibles[ps]:
                table += f'<td class="highlight"><a href="{linkto}.html">{ft.possibles[ps]}</a></td>'
            else:
                table += f'<td><a href="{linkto}.html">{ft.possibles[ps]}</a></td>'
        table += "</tr>"

    # Main table
    # Define which entries fit the filters
    #                                   all category applies or entry has same value in category as filter
    allowed = list(
        filter(
            lambda entry: all(
                name[i] == "All" or entry[f.place] == name[i]
                for i, f in enumerate(filters)
            ),
            entries,
        )
    )

    # Add the top row
    table += "</table></div><p></p>"
    if allowed:
        table += "<table><tr>"
        for field in fields:
            if field.visible:
                table += f'<th class="sticky">{field.name}</th>'
        table += "</tr>"

        # Add the rest of the rows
        for entry in allowed:
            table += "<tr>"
            for val, field in enumerate(fields):
                if field.visible:
                    table += f"<td>{entry[val]}</td>"
            table += "</tr>"
        table += "</table>"
    else:
        table += "<p>No records with those constraints.</p>"

    ttr = HEADER.strip("\n") + table
    # Write the file
    with open(f"{path}/{f.replace('/', '').replace(path, '')}.html", "w") as f:
        f.write(ttr)

print("|")

# Cross-site thing
with open("Members.csv", "r") as f:
    members = [x.split(",")[1:] for x in f.read().split("\n")]

while [] in members:
    members.remove([])

members = list(filter(lambda x: x[1] == "TRUE", members))
members = set([x[0] for x in members])

with open("../RunnerPages/pages.txt", "r") as f:
    pages = eval(f.read())

for runner in set(pages.keys()).union(members):
    results = list(filter(lambda x: runner in x[6], datalines))
    if runner not in pages and not results:
        continue
    if results:
        table = "<table>"
        for heading in fields:
            table += f"<th>{heading.name}</th>"
        for result in results:
            table += "<tr>"
            for cell in result:
                table += f"<td>{cell}</td>"
            table += "</tr>"
        table += "</table>"
        if runner in pages:
            pages[runner]["Club Records"] = table
        else:
            pages[runner] = {"Club Records": table}
    else:
        pages[runner]["Club Records"] = "<em>No club records.</em>"

with open("../RunnerPages/pages.txt", "w") as f:
    f.write(str(pages))
